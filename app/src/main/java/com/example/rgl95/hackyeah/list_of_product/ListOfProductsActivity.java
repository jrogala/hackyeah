package com.example.rgl95.hackyeah.list_of_product;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.rgl95.hackyeah.CONST;
import com.example.rgl95.hackyeah.HackYeah;
import com.example.rgl95.hackyeah.R;
import com.example.rgl95.hackyeah.list_of_product.di.ListOfProductModule;
import com.example.rgl95.hackyeah.model.meals.ListOfProductsResponse;
import com.example.rgl95.hackyeah.model.meals.ProductInfo;
import com.example.rgl95.hackyeah.nutrients.NutrientsActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOfProductsActivity extends AppCompatActivity implements
        ListOfProductsContract.View, ListOfProductsContract.MyListListener {

    @Inject
    ListOfProductsContract.Presenter presenter;
    private ListOfProductsAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recycler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_products);
        ButterKnife.bind(this);

        ((HackYeah) getApplication()).getAppComponent().plus(new ListOfProductModule(this)).inject(this);


        setupAdapter();
    }

    private void setupAdapter() {
        adapter = new ListOfProductsAdapter(this);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);
    }


    @Override
    public void updateAdapterList(ListOfProductsResponse listOfProductsResponse) {
        adapter.updateAdapterData(listOfProductsResponse.getData());
    }

    @Override
    public void showFailureToast() {
        Toast.makeText(this, "Delete failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(ProductInfo product) {
        if (isInternetConnected()) {
            Intent intent = new Intent(this, NutrientsActivity.class);
            intent.putExtra(CONST.PRODUCT_ID, product.getId());
            intent.putExtra(CONST.PRODUCT_NAME, product.getName());
            startActivity(intent);
        } else
            Toast.makeText(this, "No intenret connection", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteIconClick(ProductInfo product) {
        presenter.deleteProduct(product);
    }

    private boolean isInternetConnected() {
        ConnectivityManager manager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        NetworkInfo info = manager != null ? manager.getActiveNetworkInfo() : null;
        return info != null && info.isConnected();
    }

}
