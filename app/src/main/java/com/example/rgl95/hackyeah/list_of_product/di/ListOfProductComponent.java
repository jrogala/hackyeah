package com.example.rgl95.hackyeah.list_of_product.di;

import com.example.rgl95.hackyeah.list_of_product.ListOfProductsActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {ListOfProductModule.class})
public interface ListOfProductComponent {

    void inject(ListOfProductsActivity listOfProductsActivity);
}
