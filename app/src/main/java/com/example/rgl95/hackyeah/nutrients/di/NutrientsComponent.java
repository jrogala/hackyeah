package com.example.rgl95.hackyeah.nutrients.di;

import com.example.rgl95.hackyeah.nutrients.NutrientsActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {NutrientsModule.class})
public interface NutrientsComponent {

    void inject(NutrientsActivity nutrientsActivity);
}
