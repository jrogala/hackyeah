package com.example.rgl95.hackyeah.model.nutrients;

import com.google.gson.annotations.SerializedName;

public class Nutrients {

    private double calories;
    @SerializedName("total_fat")
    private double totalFat;
    private double cholesterol;
    private double sodium;
    private double sugar;
    private double protein;
    private double potasium;
    private double water;


    public double getCalories() {
        return calories;
    }

    public double getTotalFat() {
        return totalFat;
    }

    public double getCholesterol() {
        return cholesterol;
    }

    public double getSodium() {
        return sodium;
    }

    public double getSugar() {
        return sugar;
    }

    public double getProtein() {
        return protein;
    }

    public double getPotasium() {
        return potasium;
    }

    public double getWater() {
        return water;
    }

}