package com.example.rgl95.hackyeah.main;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import io.reactivex.Observable;

class EditTextObservableProvider {

    private EditText editText;

    EditTextObservableProvider(EditText editText) {
        this.editText = editText;
    }

    Observable<String> createTextObservable() {

        Observable<String> textChangeObs = Observable.create(

                emitter -> {
                    TextWatcher watcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            editText.setError(null);
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    };
                    editText.addTextChangedListener(watcher);
                    emitter.setCancellable(() -> editText.removeTextChangedListener(watcher));
                }
        );

        return textChangeObs
                .filter(s -> s.length() > 0);
    }
}
