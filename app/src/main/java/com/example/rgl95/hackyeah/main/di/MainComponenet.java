package com.example.rgl95.hackyeah.main.di;

import com.example.rgl95.hackyeah.main.MainActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {MainModule.class})
public interface MainComponenet {

    void inject(MainActivity mainActivity);
}
