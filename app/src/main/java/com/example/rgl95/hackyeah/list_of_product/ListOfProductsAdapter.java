package com.example.rgl95.hackyeah.list_of_product;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rgl95.hackyeah.R;
import com.example.rgl95.hackyeah.model.meals.ProductInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOfProductsAdapter extends RecyclerView.Adapter<ListOfProductsAdapter.ViewHolder> {

    private List<ProductInfo> productInfoList = new ArrayList<>();
    private ListOfProductsContract.MyListListener listener;


    ListOfProductsAdapter(ListOfProductsContract.MyListListener listener) {
        this.listener = listener;
    }

    void updateAdapterData(List<ProductInfo> list) {
        productInfoList.clear();
        productInfoList = list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ListOfProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_product, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListOfProductsAdapter.ViewHolder viewHolder, int i) {
        viewHolder.setup(productInfoList.get(i), listener);
    }

    @Override
    public int getItemCount() {
        return productInfoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_name)
        TextView name;

        @BindView(R.id.delete_image)
        ImageView deleteImage;

        @BindView(R.id.product_weight)
        TextView productWeight;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setup(ProductInfo product, ListOfProductsContract.MyListListener listener) {
            name.setText(product.getName());
            productWeight.setText(String.format("Weight: %d", product.getWeight()));

            deleteImage.setOnClickListener(view ->
                    listener.onDeleteIconClick(product));

            itemView.setOnClickListener(view ->
                    listener.onItemClick(product));

        }
    }
}
