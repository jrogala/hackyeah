package com.example.rgl95.hackyeah.model.meals;

import java.util.List;

public class ListOfProductsResponse {

    private String message;
    private List<ProductInfo> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductInfo> getData() {
        return data;
    }

    public void setData(List<ProductInfo> data) {
        this.data = data;
    }
}
