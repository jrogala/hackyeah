package com.example.rgl95.hackyeah.model.photo;

public class Base64FormatPhotoRequest {
    private String base;

    public Base64FormatPhotoRequest(String photoRequestBase64Format) {
        this.base = photoRequestBase64Format;
    }
}