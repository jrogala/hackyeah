package com.example.rgl95.hackyeah.nutrients;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.rgl95.hackyeah.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NutrientsPresenter implements NutrientsContract.Presenter, LifecycleObserver {

    private NutrientsContract.View view;
    private ApiService api;
    private CompositeDisposable disposable;

    public NutrientsPresenter(NutrientsContract.View view, ApiService api) {
        this.view = view;
        this.api = api;

        ((LifecycleOwner) view).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        disposable = new CompositeDisposable();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onStop() {
        disposable.dispose();
    }

    @Override
    public void getNutrients(String id) {
        disposable.add(api.giveMeNutrients(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        nr ->
                                view.showPieChartNutrients(nr.getDataResponse().getNutrients())
                ));


    }

}