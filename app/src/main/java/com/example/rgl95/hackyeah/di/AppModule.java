package com.example.rgl95.hackyeah.di;

import android.content.Context;

import com.example.rgl95.hackyeah.network.ApiService;
import com.example.rgl95.hackyeah.network.RetrofitClient;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    Context providesContext() {
        return context;
    }

    @Provides
    ApiService providesRetrofit() {
        return RetrofitClient.getInstance().getApi();
    }
}
