package com.example.rgl95.hackyeah.nutrients;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.rgl95.hackyeah.CONST;
import com.example.rgl95.hackyeah.HackYeah;
import com.example.rgl95.hackyeah.R;
import com.example.rgl95.hackyeah.model.nutrients.Nutrients;
import com.example.rgl95.hackyeah.nutrients.di.NutrientsModule;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NutrientsActivity extends AppCompatActivity implements NutrientsContract.View {

    @BindView(R.id.pie_chart)
    PieChart pieChart;

    private double[] nutrientsValues = new double[8];

    @Inject
    NutrientsContract.Presenter presenter;
    private String productId;
    private String productName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_nutrients);
        ButterKnife.bind(this);

        ((HackYeah) getApplication()).getAppComponent().plus(new NutrientsModule(this)).inject(this);

        productId = String.valueOf(getIntent().getIntExtra(CONST.PRODUCT_ID, 0));
        productName = getIntent().getStringExtra(CONST.PRODUCT_NAME);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getNutrients(productId);
    }

    @Override
    public void showPieChartNutrients(Nutrients nutrients) {
        String[] nutrientsNames = getResources().getStringArray(R.array.nutrientsNames);

        nutrientsValues[0] = nutrients.getPotasium();
        nutrientsValues[1] = nutrients.getProtein();
        nutrientsValues[2] = nutrients.getSugar();
        nutrientsValues[3] = nutrients.getWater();
        nutrientsValues[4] = nutrients.getTotalFat();
        nutrientsValues[5] = nutrients.getCalories();
        nutrientsValues[6] = nutrients.getSodium();
        nutrientsValues[7] = nutrients.getCholesterol();

        ArrayList<PieEntry> pieEntries = new ArrayList<>();

        for (int i = 0; i < nutrientsValues.length; i++)
            pieEntries.add(new PieEntry((float) nutrientsValues[i], nutrientsNames[i]));

        setPieChart(pieEntries);
    }

    private void setPieChart(ArrayList<PieEntry> pieEntries) {
        PieDataSet pieDataSet = new PieDataSet(pieEntries, "");
        pieDataSet.setSliceSpace(3);
        pieDataSet.setValueTextSize(20f);
        pieDataSet.setColors(setColors());

        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.animateY(800);
        pieChart.setCenterText(productName);
        pieChart.setHoleRadius(20f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDescription(null);
        pieChart.invalidate();
    }

    private ArrayList<Integer> setColors() {
        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(Color.DKGRAY);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.BLUE);
        colors.add(Color.rgb(239, 211, 25));
        colors.add(Color.CYAN);
        colors.add(Color.MAGENTA);
        colors.add(Color.LTGRAY);

        return colors;
    }
}


