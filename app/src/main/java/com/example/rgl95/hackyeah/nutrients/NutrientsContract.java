package com.example.rgl95.hackyeah.nutrients;

import com.example.rgl95.hackyeah.model.nutrients.Nutrients;

public interface NutrientsContract {

    interface View{

        void showPieChartNutrients(Nutrients nutrients);
    }

    interface Presenter{

        void getNutrients(String id);
    }
}
