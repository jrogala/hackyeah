package com.example.rgl95.hackyeah;

public interface ProgressBarInterface {

    void showProgressBar();

    void hideProgressBar();
}
