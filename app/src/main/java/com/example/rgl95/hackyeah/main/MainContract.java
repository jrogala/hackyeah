package com.example.rgl95.hackyeah.main;

import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.rgl95.hackyeah.ProgressBarInterface;

public interface MainContract {

    interface View extends ProgressBarInterface {

        @Override
        void showProgressBar();

        @Override
        void hideProgressBar();

        void showToast(String message);

        void setMealName(String data);

        void showEditTextError(EditText editText, String error);
    }

    interface Presenter {

        void sendMeal(String name, String type, int weight);

        void uploadFile(String meal);

        boolean checkAllTerms(EditText whatFood, EditText weightOfFood, AutoCompleteTextView kindOfFood);
    }


}
