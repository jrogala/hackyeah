package com.example.rgl95.hackyeah.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rgl95.hackyeah.HackYeah;
import com.example.rgl95.hackyeah.R;
import com.example.rgl95.hackyeah.list_of_product.ListOfProductsActivity;
import com.example.rgl95.hackyeah.main.di.MainModule;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.reactivex.Observable;

import static com.example.rgl95.hackyeah.CONST.LIST_ACTIVITY_REQUSET_CODE;
import static com.example.rgl95.hackyeah.CONST.MEAL_KIND;
import static com.example.rgl95.hackyeah.CONST.REQUEST_CAMERA_PERMISSION;
import static com.example.rgl95.hackyeah.CONST.REQUEST_IMAGE_CAPTURE;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.what_edit_text)
    EditText whatFood;

    @BindView(R.id.weight_edit_text)
    EditText weightOfFood;

    @BindView(R.id.kind_auto_complete_text_view)
    AutoCompleteTextView kindOfFood;

    @BindView(R.id.image_view)
    ImageView imageView;

    @BindView(R.id.progress_circular)
    ProgressBar progressBar;

    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        ((HackYeah) getApplication()).getAppComponent().plus(new MainModule(this)).inject(this);

        setupAutoComplete();
        checkPermissions();
        hideError();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_nutrients_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ListOfProductsActivity.class));
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    public void onFabCLick() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @OnTouch(R.id.kind_auto_complete_text_view)
    public boolean onTouch() {
        kindOfFood.showDropDown();
        return true;
    }

    @OnClick(R.id.send_button)
    public void sendMeal() {
        if (presenter.checkAllTerms(whatFood, weightOfFood, kindOfFood)) {
            presenter.sendMeal(whatFood.getText().toString(), kindOfFood.getText().toString(), Integer.parseInt(weightOfFood.getText().toString()));
            startActivityForResult(new Intent(this, ListOfProductsActivity.class), LIST_ACTIVITY_REQUSET_CODE);
            showProgressBar();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);

            presenter.uploadFile(convertPhotoToBase64(imageBitmap));
            showProgressBar();
        }
    }


    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        hideProgressBar();
    }

    @Override
    public void setMealName(String data) {
        whatFood.setText(data);
    }

    @Override
    public void showEditTextError(EditText editText, String error) {
        editText.setError(error);
    }

    private void hideError() {
        Observable<String> kindOfMealObserver = new EditTextObservableProvider(kindOfFood).createTextObservable();
        kindOfMealObserver.subscribe();
    }

    private void setupAutoComplete() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, MEAL_KIND);
        kindOfFood.setAdapter(adapter);
    }

    private void checkPermissions() {
        String[] permission = new String[]{Manifest.permission.CAMERA};
        if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, permission, REQUEST_CAMERA_PERMISSION);
        }
    }

    private String convertPhotoToBase64(Bitmap imageBitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (imageBitmap != null) {
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

}