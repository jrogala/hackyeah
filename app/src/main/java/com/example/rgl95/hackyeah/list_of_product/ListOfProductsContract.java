package com.example.rgl95.hackyeah.list_of_product;


import com.example.rgl95.hackyeah.model.meals.ListOfProductsResponse;
import com.example.rgl95.hackyeah.model.meals.ProductInfo;

public interface ListOfProductsContract {

    interface View {

        void updateAdapterList(ListOfProductsResponse listOfProductsResponse);

        void showFailureToast();
    }

    interface Presenter {

        void deleteProduct(ProductInfo product);
    }

    interface MyListListener {

        void onItemClick(ProductInfo product);

        void onDeleteIconClick(ProductInfo product);
    }
}
