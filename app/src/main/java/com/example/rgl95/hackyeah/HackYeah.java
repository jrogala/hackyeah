package com.example.rgl95.hackyeah;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.rgl95.hackyeah.di.AppComponent;
import com.example.rgl95.hackyeah.di.AppModule;
import com.example.rgl95.hackyeah.di.DaggerAppComponent;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public class HackYeah extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree() {
                @NonNull
                @Override
                protected String createStackElementTag(@NotNull StackTraceElement element) {
                    return super.createStackElementTag(element) + " *** " + element.getLineNumber() + " " + element.getClassName();
                }

            });

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
