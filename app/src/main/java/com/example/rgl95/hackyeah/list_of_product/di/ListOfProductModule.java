package com.example.rgl95.hackyeah.list_of_product.di;

import com.example.rgl95.hackyeah.list_of_product.ListOfProductsContract;
import com.example.rgl95.hackyeah.list_of_product.ListOfProductsPresenter;
import com.example.rgl95.hackyeah.network.ApiService;

import dagger.Module;
import dagger.Provides;

@Module
public class ListOfProductModule {

    private ListOfProductsContract.View view;

    public ListOfProductModule(ListOfProductsContract.View view) {
        this.view = view;
    }

    @Provides
    ListOfProductsContract.Presenter providesPresenter(ApiService apiService) {
        return new ListOfProductsPresenter(view, apiService);
    }
}
