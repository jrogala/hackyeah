package com.example.rgl95.hackyeah.main;


import android.support.annotation.NonNull;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.rgl95.hackyeah.model.photo.Base64FormatPhotoRequest;
import com.example.rgl95.hackyeah.model.photo.ResponsePhotoDataItem;
import com.example.rgl95.hackyeah.network.ApiService;
import com.example.rgl95.hackyeah.network.RetrofitClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private ApiService api;


    public MainPresenter(MainContract.View view, ApiService api) {
        this.view = view;
        this.api = api;
    }

    private boolean isEditTextNotEmpty(EditText editText) {

        if (editText.getText().toString().isEmpty()) {
            view.showEditTextError(editText, "It's empty");
            return false;
        } else
            return true;
    }

    @Override
    public boolean checkAllTerms(EditText whatFood, EditText weightOfFood, AutoCompleteTextView kindOfFood) {
        return (isEditTextNotEmpty(whatFood) & isEditTextNotEmpty(weightOfFood) & isEditTextNotEmpty(kindOfFood));
    }

    @Override
    public void uploadFile(String meal) {
        Call<ResponsePhotoDataItem> bodyCall = api.uploadFile(new Base64FormatPhotoRequest(meal));

        bodyCall.enqueue(new Callback<ResponsePhotoDataItem>() {

            @Override
            public void onResponse(@NonNull Call<ResponsePhotoDataItem> call, @NonNull Response<ResponsePhotoDataItem> response) {
                if (response.body() != null) {
                    view.setMealName(response.body().getData());
                    view.hideProgressBar();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponsePhotoDataItem> call, @NonNull Throwable t) {
                view.showToast("Photo wasn't sent");
                Timber.d(t);
            }
        });
    }

    @Override
    public void sendMeal(String name, String type, int weight) {
        Call<ResponseBody> responseBodyCall = api.sendMeal(name, type, weight);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                view.showToast("Meal is sent successfully");
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                view.showToast("Meal wasn't sent");
                Timber.d(t);
            }
        });
    }
}