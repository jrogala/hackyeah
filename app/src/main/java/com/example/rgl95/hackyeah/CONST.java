package com.example.rgl95.hackyeah;

public interface CONST {

    String[] MEAL_KIND = new String[]{"meal", "drink"};
    String PRODUCT_ID = "productId";
    String PRODUCT_NAME = "productName";
    int REQUEST_IMAGE_CAPTURE = 1001;
    int REQUEST_CAMERA_PERMISSION = 1002;
    int LIST_ACTIVITY_REQUSET_CODE = 1003;

}

