package com.example.rgl95.hackyeah.main.di;

import com.example.rgl95.hackyeah.main.MainContract;
import com.example.rgl95.hackyeah.main.MainPresenter;
import com.example.rgl95.hackyeah.network.ApiService;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    private MainContract.View view;

    public MainModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    MainContract.Presenter providesPresenter(ApiService apiService){
        return new MainPresenter(view, apiService);
    }
}
