package com.example.rgl95.hackyeah.list_of_product;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.support.annotation.NonNull;

import com.example.rgl95.hackyeah.model.meals.ProductInfo;
import com.example.rgl95.hackyeah.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ListOfProductsPresenter implements ListOfProductsContract.Presenter, LifecycleObserver {

    private ListOfProductsContract.View view;
    private ApiService api;
    private CompositeDisposable disposable;

    public ListOfProductsPresenter(ListOfProductsContract.View view, ApiService api) {
        this.view = view;
        this.api = api;

        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    private void getListOfProducts() {
        disposable.add(api.getList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response ->
                                view.updateAdapterList(response)
                ));
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        disposable = new CompositeDisposable();
        getListOfProducts();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onStop() {
        disposable.dispose();
    }


    @Override
    public void deleteProduct(ProductInfo product) {
        Call<Void> deleteCall = api.deleteProduct(product.getId());

        deleteCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                getListOfProducts();
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                view.showFailureToast();
                Timber.d(t);
            }
        });
    }

}
