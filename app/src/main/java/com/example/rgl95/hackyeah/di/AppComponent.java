package com.example.rgl95.hackyeah.di;

import com.example.rgl95.hackyeah.MyScope;
import com.example.rgl95.hackyeah.list_of_product.di.ListOfProductComponent;
import com.example.rgl95.hackyeah.list_of_product.di.ListOfProductModule;
import com.example.rgl95.hackyeah.main.di.MainComponenet;
import com.example.rgl95.hackyeah.main.di.MainModule;
import com.example.rgl95.hackyeah.nutrients.di.NutrientsComponent;
import com.example.rgl95.hackyeah.nutrients.di.NutrientsModule;

import dagger.Component;

@MyScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    MainComponenet plus(MainModule mainModule);

    ListOfProductComponent plus(ListOfProductModule listOfProductModule);

    NutrientsComponent plus(NutrientsModule nutrientsModule);
}
