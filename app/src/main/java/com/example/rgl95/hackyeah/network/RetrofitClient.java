package com.example.rgl95.hackyeah.network;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClient {

    private Retrofit retrofit;
    private static RetrofitClient retrofitInstance;

    private RetrofitClient() {

        retrofit = new Retrofit
                .Builder()
                .baseUrl(ApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {

        if (retrofitInstance == null)
            retrofitInstance = new RetrofitClient();

        return retrofitInstance;
    }

    public ApiService getApi() {
        return retrofit.create(ApiService.class);
    }
}
