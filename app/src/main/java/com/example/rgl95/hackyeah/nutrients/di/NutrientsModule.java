package com.example.rgl95.hackyeah.nutrients.di;

import com.example.rgl95.hackyeah.network.ApiService;
import com.example.rgl95.hackyeah.nutrients.NutrientsContract;
import com.example.rgl95.hackyeah.nutrients.NutrientsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NutrientsModule {

    private NutrientsContract.View view;

    public NutrientsModule(NutrientsContract.View view) {
        this.view = view;
    }

    @Provides
    NutrientsContract.Presenter providesPresenter(ApiService apiService) {
        return new NutrientsPresenter(view, apiService);
    }
}
