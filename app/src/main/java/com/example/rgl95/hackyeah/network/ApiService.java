package com.example.rgl95.hackyeah.network;

import com.example.rgl95.hackyeah.model.meals.ListOfProductsResponse;
import com.example.rgl95.hackyeah.model.nutrients.NutrientsResponse;
import com.example.rgl95.hackyeah.model.photo.Base64FormatPhotoRequest;
import com.example.rgl95.hackyeah.model.photo.ResponsePhotoDataItem;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    String BASE_URL = "http://hckyea.herokuapp.com/api/";

    @FormUrlEncoded
    @POST("meals")
    Call<ResponseBody> sendMeal(
            @Field("meal[name]") String name,
            @Field("meal[meal_type]") String type,
            @Field("meal[weight]") int weight
    );

    @GET("meals")
    Single<ListOfProductsResponse> getList();

    @POST("files")
    Call<ResponsePhotoDataItem> uploadFile(
            @Body() Base64FormatPhotoRequest file);

    @GET("meals/{id}")
    Single<NutrientsResponse> giveMeNutrients(@Path("id") String id);

    @DELETE("meals/{id}")
    Call<Void> deleteProduct(@Path("id") int id);


}

